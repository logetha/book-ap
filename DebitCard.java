package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;

public class DebitCard extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DebitCard frame = new DebitCard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DebitCard() {
		setTitle("Card Payment");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(221, 160, 221));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Deliver Address");
		lblNewLabel.setFont(new Font("Stencil", Font.ITALIC, 16));
		lblNewLabel.setBounds(10, 16, 189, 20);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(10, 70, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(204, 70, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Credit Card");
		lblNewLabel_1.setFont(new Font("Stencil", Font.ITALIC, 16));
		lblNewLabel_1.setBounds(5, 130, 136, 20);
		contentPane.add(lblNewLabel_1);
		
		textField_3 = new JTextField();
		textField_3.setBounds(10, 193, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(126, 193, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Street");
		lblNewLabel_2.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_2.setBounds(10, 47, 46, 23);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("City");
		lblNewLabel_3.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_3.setBounds(106, 48, 46, 23);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_5 = new JLabel("Holder Name");
		lblNewLabel_5.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_5.setBounds(10, 161, 104, 23);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Card Number");
		lblNewLabel_6.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_6.setBounds(118, 161, 94, 23);
		contentPane.add(lblNewLabel_6);
		
		textField_5 = new JTextField();
		textField_5.setBounds(106, 70, 88, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Postal Code ");
		lblNewLabel_7.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_7.setBounds(204, 47, 88, 23);
		contentPane.add(lblNewLabel_7);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(245, 193, 86, 20);
		contentPane.add(textField_6);
		
		JLabel lblSecurityKey = new JLabel("Security Key");
		lblSecurityKey.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 14));
		lblSecurityKey.setBounds(235, 162, 92, 21);
		contentPane.add(lblSecurityKey);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 14));
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		int i = 0;
				if(i>0){
					JOptionPane.showMessageDialog(DebitCard.this,"Librarian added successfully!");
					//AdminSuccess.main(new String[]{});
					//frame.dispose();
					
				}else{
					
				}
				Welcome.main(new String[] {});
			}
		});
		btnSubmit.setBounds(330, 227, 94, 23);
		contentPane.add(btnSubmit);
		
		
	}

}
