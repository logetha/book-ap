package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JLabel;

public class Category extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Category frame = new Category();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Category() {
		setTitle("Category Selection");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(221, 160, 221));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnCdsCollection = new JButton("CDs ");
		btnCdsCollection.setFont(new Font("Sylfaen", Font.BOLD, 16));
		btnCdsCollection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectCD.main(new String[]{});
			}
		});
		btnCdsCollection.setBounds(68, 43, 112, 43);
		contentPane.add(btnCdsCollection);
		
		JButton btnSoftwareCollection = new JButton("Softwares");
		btnSoftwareCollection.setFont(new Font("Sylfaen", Font.BOLD, 16));
		btnSoftwareCollection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectBooks.main(new String[]{});
			}
		});
		btnSoftwareCollection.setBounds(68, 170, 112, 43);
		contentPane.add(btnSoftwareCollection);
		
		JButton btnBooksCollection = new JButton("Books ");
		btnBooksCollection.setFont(new Font("Sylfaen", Font.BOLD, 16));
		btnBooksCollection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectBooks.main(new String[]{});
			}
		});
		btnBooksCollection.setBounds(68, 107, 112, 43);
		contentPane.add(btnBooksCollection);
		
		JLabel lblNewLabel = new JLabel("");
		Image img =new ImageIcon(this.getClass().getResource("/book.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(229, 31, 215, 183);
		contentPane.add(lblNewLabel);
		
		
	}

}
