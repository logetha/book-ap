package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class OrderConfirm extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderConfirm frame = new OrderConfirm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OrderConfirm() {
		double TotalPrice = 0;
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 570, 409);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(221, 160, 221));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		String data[][] = null;
		String column[] = null;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from books where BookId=?",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ps.setInt(1, SelectBooks.bookId);
			ResultSet rs = ps.executeQuery();
			rs.last();
			TotalPrice = Double.parseDouble(rs.getString(6)) * SelectBookView.qty;
			rs.beforeFirst();

			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			column = new String[cols];
			for (int i = 1; i <= cols; i++) {
				column[i - 1] = rsmd.getColumnName(i);
			}

			rs.last();
			int rows = rs.getRow();
			rs.beforeFirst();

			data = new String[rows][cols];
			int count = 0;
			while (rs.next()) {
				for (int i = 1; i <= cols; i++) {
					data[count][i - 1] = rs.getString(i);
				}
				count++;
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		contentPane.setLayout(null);

		table = new JTable(data, column);
		JScrollPane sp = new JScrollPane(table);
		sp.setBounds(5, 5, 424, 225);

		contentPane.add(sp);

		JButton btnBuyNow = new JButton("Confirm");
		btnBuyNow.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 16));
		btnBuyNow.setBounds(439, 313, 105, 23);
		btnBuyNow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BillPrint.main(new String[] {});
			}
		});
		contentPane.add(btnBuyNow);

		textField = new JTextField();
		textField.setBounds(439, 86, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(439, 153, 86, 20);
		textField_1.setColumns(10);
		contentPane.add(textField_1);
		textField.setText(String.valueOf(SelectBookView.qty));
		textField_1.setText(String.valueOf(TotalPrice));
		
		lblNewLabel = new JLabel("Quantity");
		lblNewLabel.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblNewLabel.setBounds(439, 52, 86, 23);
		contentPane.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Total Amount");
		lblNewLabel_1.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblNewLabel_1.setBounds(439, 117, 105, 25);
		contentPane.add(lblNewLabel_1);
	}
}
