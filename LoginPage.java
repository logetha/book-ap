package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Window.Type;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;

public class LoginPage extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	static String nameUser;
	private JLabel label;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginPage frame = new LoginPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginPage() {
		setTitle("Login Form");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(221, 160, 221));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(78, 78, 192, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(78, 145, 192, 20);
		contentPane.add(passwordField);

		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setFont(new Font("Sylfaen", Font.BOLD, 16));
		lblUserName.setBounds(45, 53, 97, 14);
		contentPane.add(lblUserName);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Sylfaen", Font.BOLD, 16));
		lblPassword.setBounds(45, 114, 89, 20);
		contentPane.add(lblPassword);

		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = textField.getText();
				String password = String.valueOf(passwordField.getPassword());
				nameUser = name;
				// System.out.println(name+" "+password);
				if (UserDB.validate(name, password)) {
					// SearchBooks.main(new String[]{});
					Category.main(new String[] {});

				} else {
					JOptionPane.showMessageDialog(LoginPage.this, "Sorry, Username or Password Error", "Login Error!",
							JOptionPane.ERROR_MESSAGE);
					textField.setText("");
					passwordField.setText("");
				}

			}
		});
		btnSubmit.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 16));
		btnSubmit.setBounds(308, 202, 89, 23);
		contentPane.add(btnSubmit);
		
		label = new JLabel("");
		Image img =new ImageIcon(this.getClass().getResource("/login.png")).getImage();
		label.setIcon(new ImageIcon(img));
		label.setBounds(291, 45, 116, 134);
		contentPane.add(label);
		
		
	}
}
