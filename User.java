package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class User extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JPasswordField passwordField;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					User frame = new User();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public User() {
		setTitle("User Info");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(221, 160, 221));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblUserName.setBounds(10, 0, 80, 20);
		contentPane.add(lblUserName);
		
		JLabel lblUserId = new JLabel("User ID");
		lblUserId.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblUserId.setBounds(10, 35, 70, 20);
		contentPane.add(lblUserId);
		
		JLabel lblEmailId = new JLabel("Email ID");
		lblEmailId.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblEmailId.setBounds(10, 74, 70, 14);
		contentPane.add(lblEmailId);
		
		JLabel lblPhoneNumber = new JLabel("Phone Number");
		lblPhoneNumber.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblPhoneNumber.setBounds(10, 111, 99, 23);
		contentPane.add(lblPhoneNumber);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblPassword.setBounds(10, 149, 80, 14);
		contentPane.add(lblPassword);
		
		textField = new JTextField();
		textField.setBounds(140, 0, 119, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(140, 35, 119, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(140, 71, 119, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(140, 112, 119, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(140, 146, 119, 20);
		contentPane.add(passwordField);
		
		JButton btnOk = new JButton("Sign up");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String userId=textField_1.getText();		
				String name=textField.getText();
				String password=String.valueOf(passwordField.getPassword());
				String email=textField_2.getText();
				String address=textField_4.getText();
				String contact=textField_3.getText();

				int i=UserDB.save(userId,name, password, email, address, contact);
				if(i>0){
					JOptionPane.showMessageDialog(User.this,"Librarian added successfully!");
					//AdminSuccess.main(new String[]{});
					//frame.dispose();
					
				}else{
					JOptionPane.showMessageDialog(User.this,"Sorry, unable to save!");
				}
			}
		});
		btnOk.setFont(new Font("Stencil", Font.ITALIC, 14));
		btnOk.setBounds(55, 227, 119, 23);
		contentPane.add(btnOk);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Welcome.main(new String[]{});
				
			}
		});
		btnNewButton.setFont(new Font("Stencil", Font.ITALIC, 14));
		btnNewButton.setBounds(249, 227, 109, 23);
		contentPane.add(btnNewButton);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(140, 182, 119, 20);
		contentPane.add(textField_4);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblAddress.setBounds(10, 185, 80, 14);
		contentPane.add(lblAddress);
	}

}
