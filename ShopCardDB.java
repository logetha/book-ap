package bookstore;

import java.sql.*;

public class ShopCardDB {

	public static boolean checkBook(int bookId) {
		boolean status = false;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from books where bookId=?");
			ps.setInt(1, bookId);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int save(int bookId) {
		int status = 0;
		String bName;
		double bTPrice;
		try {
			Connection con = DB.getConnection();

			status = updatebook(bookId);// updating quantity and issue
			PreparedStatement ps1 = con.prepareStatement("select bookTitle,Price from books where Bookid = ?");
			ps1.setInt(1, bookId);
			ResultSet rs = ps1.executeQuery();
			rs.last();
			bName = rs.getString(1);
			bTPrice = Double.parseDouble(rs.getString(2));
			rs.beforeFirst();
			if (status > 0) {
				PreparedStatement ps = con
						.prepareStatement("insert into shopCard(bookName,Quantity,TotalPrice) values(?,?,?)");
				ps.setString(1, bName);
				ps.setInt(2, SelectBookView.qty);
				ps.setDouble(3, bTPrice);

				status = ps.executeUpdate();
			}

			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int updatebook(int bookId) {
		int status = 0;
		int quantity = 0, issued = 0;
		try {
			Connection con = DB.getConnection();

			PreparedStatement ps = con.prepareStatement("select quantity from books where bookId=?");
			ps.setInt(1, bookId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				quantity = rs.getInt("quantity");
				// issued=rs.getInt("issued");
			}

			if (quantity > 0) {
				PreparedStatement ps2 = con.prepareStatement("update books set quantity=? where bookId=?");
				ps2.setInt(1, quantity - 1);
				// ps2.setInt(2,issued+1);
				ps2.setInt(2, bookId);

				status = ps2.executeUpdate();
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}
}
