package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BillPrint extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BillPrint frame = new BillPrint();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BillPrint() {
		setTitle("Billing Receipt");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(221, 160, 221));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Customer Name");
		lblNewLabel.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 11, 125, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Book Tittle");
		lblNewLabel_1.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblNewLabel_1.setBounds(10, 50, 76, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Qty");
		lblNewLabel_2.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblNewLabel_2.setBounds(10, 85, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Price of Each Book");
		lblNewLabel_3.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblNewLabel_3.setBounds(10, 117, 141, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Total");
		lblNewLabel_4.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblNewLabel_4.setBounds(10, 154, 46, 14);
		contentPane.add(lblNewLabel_4);

		textField = new JTextField();
		textField.setBackground(new Color(255, 255, 255));
		textField.setBounds(151, 8, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(151, 47, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(151, 82, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(151, 114, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(151, 151, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		JButton btnNewButton = new JButton("Next");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DebitCard.main(new String[]{});
			}
		});
		btnNewButton.setFont(new Font("Stencil", Font.ITALIC, 14));
		btnNewButton.setBounds(297, 207, 127, 23);
		contentPane.add(btnNewButton);
		textField.setText(LoginPage.nameUser);
		textField_2.setText(String.valueOf(SelectBookView.qty));

		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select bookTitle,price from books where bookId=?");
			ps.setInt(1, SelectBooks.bookId);
			ResultSet rs = ps.executeQuery();
			rs.last();
			textField_1.setText(rs.getString(1));
			textField_3.setText(rs.getString(2));

			rs.beforeFirst();
			double priceAdd = Double.parseDouble(textField_3.getText()) * SelectBookView.qty;
			textField_4.setText(String.valueOf(priceAdd));

			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
