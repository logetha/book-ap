package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class SelectBookView extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	static int qty;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SelectBookView frame = new SelectBookView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SelectBookView() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 409);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(221, 160, 221));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		String data[][] = null;
		String column[] = null;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from books where BookId=?",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ps.setInt(1, SelectBooks.bookId);
			ResultSet rs = ps.executeQuery();

			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			column = new String[cols];
			for (int i = 1; i <= cols; i++) {
				column[i - 1] = rsmd.getColumnName(i);
			}

			rs.last();
			int rows = rs.getRow();
			rs.beforeFirst();

			data = new String[rows][cols];
			int count = 0;
			while (rs.next()) {
				for (int i = 1; i <= cols; i++) {
					data[count][i - 1] = rs.getString(i);
				}
				count++;
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		contentPane.setLayout(null);

		table = new JTable(data, column);
		JScrollPane sp = new JScrollPane(table);
		sp.setBounds(5, 5, 424, 225);

		contentPane.add(sp);

		JButton btnBuyNow = new JButton("Buy Now");
		btnBuyNow.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 16));
		btnBuyNow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				qty = Integer.parseInt(textField.getText());
				OrderConfirm.main(new String[] {});
			}
		});
		btnBuyNow.setBounds(266, 305, 163, 33);
		contentPane.add(btnBuyNow);

		JButton btnAddToShopping = new JButton("Add To Shopping Card");
		btnAddToShopping.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 16));
		btnAddToShopping.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int bookId = SelectBooks.bookId;
				qty = Integer.parseInt(textField.getText());

				if (ShopCardDB.checkBook(bookId)) {

					int i = ShopCardDB.save(bookId);
					if (i > 0) {
						JOptionPane.showMessageDialog(SelectBookView.this, "Book added successfully!");
						ShopCard.main(new String[] {});
						// frame.dispose();

					} else {
						JOptionPane.showMessageDialog(SelectBookView.this, "Sorry, unable to issue!");
					} // end of save if-else

				} else {
					JOptionPane.showMessageDialog(SelectBookView.this, "Sorry, Callno doesn't exist!");
				} // end of checkbook if-else
			}
		});
		btnAddToShopping.setBounds(23, 305, 222, 33);
		contentPane.add(btnAddToShopping);

		textField = new JTextField();
		textField.setBounds(255, 250, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblEnterQuantity = new JLabel("Enter Quantity");
		lblEnterQuantity.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 16));
		lblEnterQuantity.setBounds(127, 244, 118, 33);
		contentPane.add(lblEnterQuantity);
	}
}
